// Based on https://c9x.me/articles/gthreads/code0.html
#include "gthr.h"

// Dummy function to simulate some thread work
void f(void) {
  static int x;
  int i = 0, id;

  id = ++x;
  while (true) {

    printf("F Thread id = %d, val = %d BEGINNING\n", id, ++i);
    uninterruptibleNanoSleep(0, 50000000);
    printf("F Thread id = %d, val = %d END\n", id, ++i);
    uninterruptibleNanoSleep(0, 50000000);
  }
}

// Dummy function to simulate some thread work
void g(void) {
  static int x;
  int i = 0, id;

  id = ++x;
  while (true) {

    printf("G Thread id = %d, val = %d BEGINNING\n", id, ++i);
    uninterruptibleNanoSleep(0, 50000000);
    printf("G Thread id = %d, val = %d END\n", id, ++i);
    uninterruptibleNanoSleep(0, 50000000);

  }
}

void p(void) {
  const int threadId = 2;
  const int newPriority = 20;
  const int sleepMs = 1000;
  
  printf("Starting priority controller for thread %d...\n", threadId);
  printf("Keep running for %d ms\n", sleepMs);
  gtsleep(sleepMs);

  printf("Priority controller woke up!\n");
  gtstats();
  printf("Switching priority of thread %d to %d\n", threadId, newPriority);
  gtsetpriority(threadId, newPriority);
  gtresetstats(threadId);
  printf("Keep running for %d ms\n", sleepMs);
  gtsleep(sleepMs);

  printf("Priority controller woke up!\n");
  gtstats();
  printf("Priority controller exitting...\n");
}

int main(void) {
  gtinit(Lottery);	  	// initialize threads, see gthr.c
  gtgo(f, 1);		// set f() as first thread
  gtgo(f, 2);		// set f() as second thread
  gtgo(g, 3);		// set g() as third thread
  gtgo(g, 9);		// set g() as fourth thread
  gtgo(f, 5);		// set f() as second thread
  gtgo(g, 2);		// set g() as third thread
  gtgo(p, 1);    // dynamically switches priorities
  gtret(1); 		// wait until all threads terminate
}
