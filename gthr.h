#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

enum {
    MaxGThreads = 10,		// Maximum number of threads, used as array size for gttbl
    StackSize = 0x400000,	// Size of stack of each thread
    TopPriority = 0,
    LeastPriority = 10,
};

enum Scheduler {
  RoundRobin,
  PrioritizedRoundRobin,
  Lottery,
};

struct gt {
  // Saved context, switched by gtswtch.S (see for detail)
  struct gtctx {
    uint64_t rsp;
    uint64_t r15;
    uint64_t r14;
    uint64_t r13;
    uint64_t r12;
    uint64_t rbx;
    uint64_t rbp;
  } ctx;
  // Thread state
  enum {
    Unused,
    Running,
    Asleep,
    Ready,
  } st;

  // statistics
  clock_t runStart;   // latest run start
  clock_t runEnd;     // latest run end
  double runTime;   // thread run time [sec]
  unsigned long rounds;
  // wait time [sec]
  double min;
  double max;
  double avg;

  // multi-purpose priority specification
  int priority;
  float tmpPriority;

  // sleep mechanics
  struct timespec sleepStart;
  int sleepDuration;   // [ms]
};

struct gt gttbl[MaxGThreads];	// statically allocated table for thread control
struct gt * gtcur;				// pointer to current thread
enum Scheduler gtsched; // selected scheduler to use

void gtinit(enum Scheduler s);				// initialize gttbl
void gtret(int ret);			// terminate thread
void gtswtch(struct gtctx * old, struct gtctx * new);	// declaration from gtswtch.S
bool gtyield(void);				// yield and switch to another thread
void gtstop(void);				// terminate current thread
int gtgo(void( * f)(void), int priority);		// create new thread and set f as new "run" function
void resetsig(int sig);			// reset signal
void gthandle(int sig);			// periodically triggered by alarm
int uninterruptibleNanoSleep(time_t sec, long nanosec);	// uninterruptible sleep
void gtsetpriority(int threadId, int priority);   // dynamic priority setting
void gtsleep(int ms);   // thread can put itself to sleep
void gtresetstats(int threadId);    // resets statistics records

const char * gtstates[4]; // state conversion to string
void gtstats();       // show statistics table
